package assignments;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Assignment1Task5 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Ritik Jain\\eclipse-workspace\\SeleniumTrain\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/select-menu/");
		driver.manage().window().maximize();
		
		
		WebElement e = driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]"));
		e.click();
		driver.findElement(By.xpath("//div[contains(text(),'Group 1, option 1')]")).click();
		
		
		WebElement secondOption = driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]"));
		secondOption.click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[contains(text(),'Mr.')]")).click();
		Thread.sleep(3000);
		
		
		WebElement thirdOption = driver.findElement(By.xpath("//select[@id='oldSelectMenu']"));
		thirdOption.click();
		Thread.sleep(3000);
		thirdOption.sendKeys(Keys.ARROW_DOWN);
		thirdOption.sendKeys(Keys.TAB);
		Thread.sleep(3000);
		
		
		WebElement fourthOption = driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/div[7]/div[1]/div[1]/div[1]/div[2]/div[1]/*[1]"));
		fourthOption.click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[contains(text(),'Black')]")).click();
		
		
		WebElement fifthOption = driver.findElement(By.id("cars"));
		Select cars = new Select(fifthOption);
		cars.selectByVisibleText("Audi");

	}

}

