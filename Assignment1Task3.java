package assignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Assignment1Task3 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Ritik Jain\\eclipse-workspace\\SeleniumTrain\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/droppable/");
		driver.manage().window().maximize();		
		WebElement from=driver.findElement(By.id("draggable"));
		WebElement to=driver.findElement(By.id("droppable"));
		Actions act=new Actions(driver);
		act.dragAndDrop(from,to).build().perform();
		if(driver.findElement(By.xpath("//p[contains(text(),'Dropped!')]")).isDisplayed())							
     	{		
         	System.out.println("Dropped! Displayed !!!");					
     	}
		else
     	{
        	System.out.println("Dropped! not Displayed !!!");					
     	}		
}
	}
