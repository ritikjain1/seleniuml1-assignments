package assignments;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class Assignment1Task2 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Ritik Jain\\eclipse-workspace\\SeleniumTrain\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/automation-practice-form");
		driver.manage().window().maximize();
		driver.findElement(By.cssSelector("#firstName")).sendKeys("Ritik");
		driver.findElement(By.cssSelector("#lastName")).sendKeys("Jain");
		driver.findElement(By.xpath("//label[contains(text(),'Male')]")).click();
		driver.findElement(By.cssSelector("#userNumber")).sendKeys("1234567890");
		
		
		WebElement e = driver.findElement(By.xpath("//*[@id=\"subjectsInput\"]"));
		e.click();
		e.sendKeys("maths");
		e.sendKeys(Keys.ENTER);
		
		driver.findElement(By.xpath("//button[@id='submit']")).submit();
		
		
		System.out.println("Tasks Done!");

	}

}
