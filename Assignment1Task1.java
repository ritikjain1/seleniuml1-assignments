package assignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment1Task1 {
	public static void main(String[] args){
	
	System.setProperty("webdriver.chrome.driver","C:\\Users\\Ritik Jain\\eclipse-workspace\\SeleniumTrain\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://demoqa.com/selectable/");
	driver.manage().window().maximize();
	driver.findElement(By.xpath("//li[contains(text(),'Cras justo odio')]")).click();
	String item1 = driver.findElement(By.xpath("//li[contains(text(),'Cras justo odio')]")).getText();
	System.out.println(item1);
	driver.findElement(By.xpath("//li[contains(text(),'Dapibus ac facilisis in')]")).click();
	String item2 = driver.findElement(By.xpath("//li[contains(text(),'Dapibus ac facilisis in')]")).getText();
	System.out.println(item2);
	driver.findElement(By.xpath("//li[contains(text(),'Morbi leo risus')]")).click();
	String item3 = driver.findElement(By.xpath("//li[contains(text(),'Morbi leo risus')]")).getText();
	System.out.println(item3);
	driver.findElement(By.xpath("//li[contains(text(),'Porta ac consectetur ac')]")).click();
	String item4 = driver.findElement(By.xpath("//li[contains(text(),'Porta ac consectetur ac')]")).getText();
	System.out.println(item4);

}

}