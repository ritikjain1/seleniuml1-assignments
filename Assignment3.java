package assignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Assignment3 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Ritik Jain\\eclipse-workspace\\SeleniumTrain\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.makemytrip.com/");
		driver.manage().window().maximize();
		//Thread.sleep(2000);
		//driver.findElement(By.xpath("/html/body/div/div/div[1]/div[1]/div[1]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[1]/div[1]/ul/li[1]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'From')]")).click();
		WebElement from = driver.findElement(By.xpath("//input[@placeholder='From']"));
		from.click();
		from.sendKeys("Bhopal");
		Thread.sleep(2000);
		
		from.sendKeys(Keys.ARROW_DOWN);
		
		from.sendKeys(Keys.ENTER);
		
		
		driver.findElement(By.xpath("//span[contains(text(),'To')]")).click();
		WebElement to =driver.findElement(By.xpath("//input[@placeholder='To']"));
		to.sendKeys("Delhi");
		Thread.sleep(2000);
		
		to.sendKeys(Keys.ARROW_DOWN);
		
		to.sendKeys(Keys.ENTER);
		
		driver.findElement(By.xpath("//span[contains(text(),'DEPARTURE')]")).click();
		driver.findElement(By.xpath("/html/body/div/div/div[2]/div/div[1]/div[2]/div[1]/div[3]/div[1]/div/div/div/div[2]/div/div[2]/div[2]/div[3]/div[3]/div[3]")).click();
		driver.findElement(By.xpath("/html/body/div/div/div[2]/div/div[1]/div[2]/p/a")).click();
		
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div[2]/div[2]/div/span")).click();
		
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div/div/div[1]/div[1]/div[2]/div[2]/div/button/span[2]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[contains(text(), 'Log in')];")).click();
		
	
	}

}
