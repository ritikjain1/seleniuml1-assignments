package assignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Assignment2Test {
	
	
	@Test 
	public void successLogin(){
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Ritik Jain\\eclipse-workspace\\SeleniumTrain\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.olay.com/login.php");
		driver.manage().window().maximize();
		
		
		driver.findElement(By.xpath("//input[@id='login_email']")).sendKeys("kevinparker@gmail.com");
		driver.findElement(By.xpath("//input[@id='login_pass']")).sendKeys("kevinparker1");
		driver.findElement(By.xpath("//input[@title='click here to sign in to your account']")).click();
		
		driver.close();
	}
	
	@Test
	public void wrongPassword() {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Ritik Jain\\eclipse-workspace\\SeleniumTrain\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.olay.com/login.php");
		driver.manage().window().maximize();
		
		
		driver.findElement(By.xpath("//input[@id='login_email']")).sendKeys("kevinparker@gmail.com");
		driver.findElement(By.xpath("//input[@id='login_pass']")).sendKeys("kevinparker2");
		driver.findElement(By.xpath("//input[@title='click here to sign in to your account']")).click();
		
		System.out.print("Login Unsuccessful");
		driver.close();
	}
	
	

}